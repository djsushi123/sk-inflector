package djsushi

import java.io.File
import kotlin.reflect.KProperty1

/**
 * Translates word meta into readable text.
 */
object Sorter {

	// TODO make a file in "byMeta" for every single meta combination

//	val fileNameList = createFileNameList(

	/**
	 * The path to the resources directory inside the project
	 */
	lateinit var resourcesPath: String

	/**
	 * The name of the file containing the main dictionary
	 */
	lateinit var sourceFileName: String

//	private class NotInitializedException(s: String): Exception(s)
//
//	init {
//		 TODO make this work
//		if (!this::sourcePath.isInitialized)
//			throw NotInitializedException("Initialization of property 'sourcePath' is required.")
//	}


	/**
	 * Sorts and splits the files from the main dictionary into smaller files. This function can sort by either
	 * base, inflected or meta. All the sorted files go into different folders in
	 * [resourcesPath]sortedWords/by{Base/Inflected/Meta}.
	 */
	fun sort(sortBy: KProperty1<Word, *>) {
		// load the main dictionary
		val lines = File(resourcesPath + sourceFileName).readLines()

		// convert the main dictionary lines into a Word object list (each line is one Word object)
		val words = lines.map {
			val split = it.split("\t")
			Word(split[0], split[1], split[2])
		}.sortedWith(compareBy {
			when (sortBy) {
				Word::base -> it.base
				Word::inflected -> it.inflected
				Word::meta -> it.meta
				else -> throw IllegalArgumentException("sortBy argument should be one of these three: \"base\", \"inflected\" or \"meta\"")
			}
		})

		// for each Word in the List<Word>, decide the filename in which the file should go and append it to the file
		// the word list is already sorted by the [sortedWith] function so the files will also go in a sorted order
		// into the Files
		words.forEach {

			// find out the filename for the current Word
			val filename = decideFile(
				when (sortBy) {
					Word::base -> it.base
					Word::inflected -> it.inflected
					Word::meta -> it.meta
					else -> throw IllegalArgumentException("sortBy argument should be one of these three: \"base\", \"inflected\" or \"meta\"")
				}) + ".csv"

			// the File in which the Word will go into
			val file = File(
				"${resourcesPath}sortedWords/" +
						when (sortBy) {
							Word::base -> "byBase"
							Word::inflected -> "byInflected"
							Word::meta -> "byMeta"
							else -> throw IllegalArgumentException("sortBy argument should be one of these three: \"base\", \"inflected\" or \"meta\"")
						} + "/$filename"
			)

			// if the File doesn't exist, create the File
			if (!file.exists())
				file.createNewFile()

			// append the current Word in the file
			file.appendText("${it.base}\t${it.inflected}\t${it.meta}\n")
		}
	}

	// TODO is this even necessary? if not, remove
	private fun createFileNameList(): List<String> {
		val list = mutableListOf<String>()
		val abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		for (letter1 in abc) {
			list.add("$letter1")
			for (letter2 in abc) {
				list.add("${letter1}${letter2}")
			}
		}
		list.add("OTHER")
		return list.toList()
	}

	/**
	 * Returns the filename for String [s] (for example, the String "červený" should go into the file CE or the
	 * String "*žiadúci" should go into OTHER
	 */
	fun decideFile(s: String): String {
		if (s.length == 1)
			return normalizeChar(s[0]).toString()

		val firstLetter = normalizeChar(s[0]) ?: return "OTHER"
		val secondLetter = normalizeChar(s[1]) ?: return "OTHER"

		return "${firstLetter}${secondLetter}"
	}

	/**
	 * Clears the files (only removes content, not the actual files) at
	 * [resourcesPath]sortedWords/by{Base/Inflected/Meta} depending on the [clearBy] argument.
	 */
	fun clearFiles(clearBy: KProperty1<Word, *>) {
		for (file in File(resourcesPath + "sortedWords/" +
				when (clearBy) {
					Word::base -> "byBase"
					Word::inflected -> "byInflected"
					Word::meta -> "byMeta"
					else -> throw IllegalArgumentException("clearBy argument should be one of these three: \"base\", \"inflected\" or \"meta\"")
				}).listFiles()!!) {
			file.writeText("")
		}

	}

	/**
	 * Converts an accented character [char] to a normal character and returns it.
	 */
	private fun normalizeChar(char: Char): Char? {

		return when (char.toLowerCase()) {
			in "aáäà" -> 'A'
			in "b"    -> 'B'
			in "cč"   -> 'C'
			in "dď"   -> 'D'
			in "eéê"  -> 'E'
			in "f"    -> 'F'
			in "g"    -> 'G'
			in "h"    -> 'H'
			in "ií"   -> 'I'
			in "j"    -> 'J'
			in "k"    -> 'K'
			in "lĺľ"  -> 'L'
			in "m"    -> 'M'
			in "nň"   -> 'N'
			in "oóôö" -> 'O'
			in "p"    -> 'P'
			in "q"    -> 'Q'
			in "rŕř"  -> 'R'
			in "sš"   -> 'S'
			in "tť"   -> 'T'
			in "uúü"  -> 'U'
			in "v"    -> 'V'
			in "w"    -> 'W'
			in "x"    -> 'X'
			in "yý"   -> 'Y'
			in "zž"   -> 'Z'
			else      -> null
		}
	}

}