package djsushi

data class Word(val base: String, val inflected: String, val meta: String)