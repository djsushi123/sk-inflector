package djsushi.properties

enum class Case(val abbr: Char) {
    NOMINATIVE('1'),
    GENITIVE('2'),
    DATIVE('3'),
    ACCUSATIVE('4'),
    VOCATIVE('5'),
    LOCATIVE('6'),
    INSTRUMENTAL('7');

    companion object {
        fun get(abbr: Char, partOfSpeech: PartOfSpeech): Case {
            when (partOfSpeech) {
                PartOfSpeech.NOUN -> when (abbr) {
                    '1' -> return NOMINATIVE
                    '2' -> return GENITIVE
                    '3' -> return DATIVE
                    '4' -> return ACCUSATIVE
                    '5' -> return VOCATIVE
                    '6' -> return LOCATIVE
                    '7' -> return INSTRUMENTAL
                }
            }

            throw IllegalArgumentException("Abbreviation '$abbr' isn't in the list.")
        }
    }
}