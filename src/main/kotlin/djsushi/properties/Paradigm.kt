package djsushi.properties

enum class Paradigm {

    SUBSTANTIVE,
    ADJECTIVAL,
    FUSED,
    UNCOMPLETE,
    MIXED,
    PRONOMINAL,
    ADVERBIAL;

    companion object {
        fun get(abbr: Char, partOfSpeech: PartOfSpeech): Paradigm {

            val e = IllegalArgumentException("Abbreviation '$abbr' isn't in the list.")

            return when (partOfSpeech) {
                PartOfSpeech.NOUN -> when (abbr) {
                    'S' -> SUBSTANTIVE
                    'A' -> ADJECTIVAL
                    'F' -> FUSED
                    'U' -> UNCOMPLETE
                    else -> throw e
                }
                else -> throw e
            }
        }
    }

}